Download MySQL database dump at:

* Lastest (Oct 22, 2018): https://zenodo.org/record/1468869/files/gtdb_bacteria-db-backup-2018-10-22.sql.tar.gz?download=1 View meta info at https://zenodo.org/record/1468869#.W84W-xP0nOQ
* October 22, 2018: https://zenodo.org/record/1468869#.W84W-xP0nOQgtdb_bacteria-db-backup-2018-10-22.sql.tar.gz?download=1
* October 18, 2017: https://zenodo.org/record/1034451/files/phydo_db_2018_10_18.sql.tar.gz?download=1


Change Log:

* October 22, 2018: Updated to GTDB Bacteria taxonomy (r83). Includes KEGG and Pfam annotations, protein sequences.
* October 18, 2017: First version, includes Pfam v30.0 on the Hug et al. (2016) tree of life based on NCBI taxonomy.
